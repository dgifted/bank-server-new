@extends('layouts.admin-master')

@section('page-title', 'Settings')
@section('plugin-styles')
@stop
@section('page-styles')
@stop
@section('content-header', 'Setups')

@section('content')
    @if(isset($message))
        <div class="row">
            <div class="col-12 col-md-9">
                <div class="alert alert-success alert-dismissible text-light">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Prompt!</h5>
                    {{$message}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12 col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.settings')}}" method="POST"
                          class="form-horizontal">
                        @csrf

                        <div class="form-group row">
                            <label for="receiving_account"
                                   class="col-sm-2 col-form-label">{{__('Receiving Account')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="receiving_account" class="form-control" id="receiving_account"
                                       placeholder="Receiving account"
                                       value="{{$settings->receiving_account ?? ''}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="crypto_address" class="col-sm-2 col-form-label">{{__('Wallet Address')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="crypto_address" class="form-control" id="crypto_address"
                                       placeholder="Wallet address"
                                       value="{{$settings->crypto_address ?? ''}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="whatsapp_line_1"
                                   class="col-sm-2 col-form-label">{{__('Whatsapp Line 1')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="whatsapp_line_1" class="form-control" id="whatsapp_line_1"
                                       placeholder="Whatsapp Line 1"
                                       value="{{ $settings->whatsapp_line_1 ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="whatsapp_line_2"
                                   class="col-sm-2 col-form-label">{{__('Whatsapp Line 2')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="whatsapp_line_2" class="form-control" id="whatsapp_line_2"
                                       placeholder="Whatsapp Line 1"
                                       value="{{ $settings->whatsapp_line_2 ?? '' }}">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-save mr-2"></i>
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
@stop
