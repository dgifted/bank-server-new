@extends('layouts.admin-master')

@section('page-title', 'Customer details')
@section('plugin-styles')
@stop
@section('page-styles')
@stop
@section('content-header', $user->first_name . ' ' . $user->last_name . '\'s Information')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{ $user->avatar }}"
                            alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{ $user->first_name }} {{ $user->last_name }}</h3>

                    <p class="text-muted text-center">
                        <span class="text-success p-2">Verified</span>
                    </p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item mt-5">
                            <b>Balance</b> <a class="float-right">${{ $balance }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Accounts</b> <a class="float-right">{{ $user->accounts()->count() ?? 0 }}</a>
                        </li>
                    </ul>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <div class="col-md-9">
            @if (session('message'))
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    <p class="m-0 p-0">
                        {{ session('message') }}
                    </p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#transactions" data-toggle="tab">Fund
                                Account</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#balance" data-toggle="tab">Manage
                                Balance</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body  overflow-auto" style="max-height: 500px;">
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <form action="" method="POST" class="form-horizontal">
                                @csrf

                                <div class="form-group row">
                                    <label for="first_name" class="col-sm-2 col-form-label">{{ __('First Name') }}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="first_name"
                                            placeholder="Name" value="{{ $user->first_name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-sm-2 col-form-label">{{ __('Last Name') }}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="last_name"
                                            placeholder="Name" value="{{ $user->last_name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" id="email"
                                            placeholder="Email" value="{{ $user->email }}" readonly>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">{{ __('Account Type') }}</label>
                                    <div class="col-sm-10">
                                        <select name="coin" id="coin" class="form-control">
                                            @if ($accountType->count() == 0)
                                                <option>No account type available</option>
                                            @else
                                                @foreach ($accountType as $type)
                                                    {{-- {{ $user->wallets->coin_id ?? 1 == $coin->id ? 'selected' : '' }} --}}
                                                    <option value="{{ $type->id }}"
                                                        @if ($user->accounts()->first()->type == $type->title) selected @endif>
                                                        {{ $type->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fas fa-save mr-2"></i>
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane active" id="transactions">
                            <form action="{{ route('admin.customers.single.deposit', $user->uid) }}" method="POST">
                                @csrf

                                <hr class="mt-3">
                                <div class="row p-2">
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Amount</label>
                                                    <input type="number" name="amount" class="form-control" id="amount"
                                                        placeholder="Enter amount to fund" required
                                                        value="{{ old('amount') ?? '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="row mt-2 p-2">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <button type="submit" id="submit" class="btn btn-md btn-primary">
                                                Continue
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {{-- <input type="hidden" name="ref_id" value="{{$type->ref_id}}"> --}}
                            </form>
                        </div>
                        <div class="tab-pane active" id="balance">
                            <form action="{{ route('admin.customers.single.deductt', $user->uid) }}" method="POST">
                                @csrf

                                <hr class="mt-3">
                                <div class="row p-2">
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Amount</label>
                                                    <input type="number" name="amount" class="form-control" id="amount"
                                                        placeholder="Enter amount to deduct" required
                                                        value="{{ old('amount') ?? '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="row mt-2 p-2">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <button type="submit" id="submit" class="btn btn-md btn-primary">
                                                Save changes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {{-- <input type="hidden" name="ref_id" value="{{$type->ref_id}}"> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('page-plugin')
@stop
@section('page-scripts')
@stop
