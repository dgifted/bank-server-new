@extends('layouts.admin-master')

@section('page-title', 'Dashboard')
@section('plugin-styles')
@stop
@section('page-styles')
@stop
@section('content-header', 'Account Detail')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    <strong>
                        Account Number: {{ $account->number }}
                    </strong>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('message'))
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    <p class="m-0 p-0">
                        {{ session('message') }}
                    </p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title mt-2">Account Detail</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                    <h3 class="widget-user-username">{{ $account->user->first_name }}
                                        {{ $account->user->last_name }}</h3>
                                    <h5 class="widget-user-desc">{{ $account->user->email }}</h5>
                                </div>
                                <div class="widget-user-image">
                                    <img class="img-circle elevation-2" src="{{ $account->user->avatar }}"
                                        alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-sm-6 offset-md-3">
                                            <div class="description-block">
                                                <h5 class="description-header">Member since:</h5>
                                                <span class="description-text text-sm">
                                                    @if ($account->user->created_at->diffInMonths() < 1)
                                                        less than a month
                                                    @else
                                                        {{ $account->user->created_at->diffInMonths() }}
                                                    @endif
                                                </span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Balance</span>
                                            <span class="info-box-number text-center text-muted mb-0">
                                                <i class="text-uppercase">{{ $account->currency->abbreviation }}</i>
                                                {{ $account->balance }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Account status</span>
                                            <span
                                                class="info-box-number text-center mb-0">
                                                @switch($account->status_name)
                                                @case('active')
                                                    <span class="badge badge-success">Active</span>
                                                @break
                                                @case('dormant')
                                                    <span class="badge badge-warning">Dormant</span>
                                                @break
                                                @case('on-hold')
                                                    <span class="badge badge-secondary">On hold</span>
                                                @break
                                                @case('suspended')
                                                    <span class="badge badge-danger">Suspended</span>
                                                @break
                                                @case('on-hold')
                                                    <span class="badge badge-info">On hold</span>
                                                @break
                                                @default
                                                    <span class="badge badge-primary">Inactive</span>
                                            @endswitch
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Currency</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0 text-uppercase">{{ $account->currency->name }}({{ $account->currency->abbreviation }})</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Change Account Status</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.accounts.single.change-status', $account->ref_id) }}" method="POST">
                        @csrf

                        <hr class="mt-3">
                        <div class="row p-2">
                            <div class="col-12 col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control">
                                                @foreach ($statuses as $key => $value)
                                                    <option value="{{ $value }}"
                                                        @if ($account->status == $value) selected @endif>
                                                        {{ $key }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-1">
                        <div class="row mt-2 p-2">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <button type="submit" id="submit" class="btn btn-md btn-primary">
                                        Save changes
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{-- <input type="hidden" name="ref_id" value="{{$type->ref_id}}"> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
@stop
