<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Powered by <a href="mailto:j.swiftlaunchme@gmail.com">Swift Launch Nig</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2022 <a href="{{ route('home') }}" onclick="(function(e){e.preventDefault();})(event)">{{config('app.name')}} Bank &trade;</a>.</strong> All rights reserved.
</footer>
