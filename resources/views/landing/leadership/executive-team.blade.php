@extends('layouts.landing-master')
@section('page-title', 'Executive Team')
@section('page-meta')
    <meta content="Ottawa owned for over 100 years." name="description"/>
    <meta content="Executive Team | {{config('app.name')}} Bank" property="og:title"/>
    <meta content="Ottawa owned for over 100 years." property="og:description"/>
    <meta content="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d51de4e2ca59a54469245f9_BSB_SEO_Home.jpg"
          property="og:image"/>
@stop

@section('content')
    <div id="section1" class="hero-inside leadership">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Executive Team</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav"><a href="{{route('boards')}}" class="nav-link-sidebar">Board of
                    Directors</a><a
                    href="{{route('executives')}}" class="nav-link-sidebar w--current">Executive Team</a></div>
            <div class="column-right-content">
                <div class="flex-top">
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5daf1770b1ac1afffaf9729f_brian-schneider.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Brian Schnaeider</h3>
                            <p><strong>Chief Executive Officer</strong><!-- <br/>(918) 712-4712 ext. 4746<br/> -->
                                <!--< a href="mailto:bschneretyeider@bluesky.bank"><em>Schnaeider@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5daf177002af90189a7d365a_chris-wilson.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Christ Wilson</h3>
                            <p><strong>Chief Credit Officer / President</strong>
                                <!-- <br/>(918) 712-5400 ext. 4705<br/> -->
                                <!-- <a href="mailto:cwilsoewfn@bluesky.bank"><em>cwfaon@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5e464ec28d48931dd839c72c_david-story.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>David Stone</h3>
                            <p><strong>Chief Financial Officer</strong><!-- <br/>(918) 225-6750<br/> -->
                                <!-- <a href="mailto:dsdtttoyyry@bluesky.bank"><em>didyyory@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="flex-top">
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5daf17702b63963591e60924_pam-carpenter.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Pamela D. Carpenter</h3>
                            <p><strong>Chief Administrative Officer</strong><!-- <br/>(918) 287-4126 ext. 1224<br/> -->
                                <!-- <a href="mailto:pcarpewwww5nter@bluesky.bank"><em>pamelacarpenter@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5e464ece8d48933b8e39c73b_ryan-mcdaniel.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Ryan James</h3>
                            <p><strong>Executive Vice President, Managing Director Commercial Banking</strong>
                                <!-- <br/>(918) 712-7500 ext. 4740<br/> -->
                                <!-- <a href="mailto:rmcddfaniel@bluesky.bank"><em>rdfgdaniel@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                    <div class="executive margin0"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5daf1770a011172b05c1d9f3_cl-stallard.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>C. L. Stallard</h3>
                            <p><strong>Market President<br/>Cleveland &amp; Pawhuska</strong>
                                <!-- <br/>(918) 448-5004 ext. 2000<br/> -->
                                <!-- <a href="mailto:cstayeellard@bluesky.bank"><em>cstyyeallard@bluesky.bank</em></a> -->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="flex-top">
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5e464ed8479e4c7dfd4182d4_brenda-magdeburg.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Brenda Magdeburg</h3>
                            <p><strong>Market President<br/>Cushing</strong>
                            </p>
                        </div>
                    </div>
                    <div class="executive"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5e464eebf1271fa488131c58_kelly-young.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Kelly Young</h3>
                            <p><strong>Market Vice President<br/>Cushing</strong>

                            </p>
                        </div>
                    </div>
                    <div class="executive margin0"><img
                            src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5daf1772487bb6647ded78cd_louis-medina.jpg"
                            alt="" class="executive-image"/>
                        <div class="executive-bio w-richtext">
                            <h3>Louis Medina</h3>
                            <p><strong>Executive Vice President</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
