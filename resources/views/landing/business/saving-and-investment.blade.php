@extends('layouts.landing-master')
@section('page-title', 'Business Savings and Investments')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank’s Business Savings and Investment Accounts</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970pxpx" id="w-node-c7a7956f824e-768510b2"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d26030d0574ee34697671dc_Business Savings and Investments.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Your business needs a home to grow its savings safely. {{config('app.name')}} Bank will guide you to the right
                        place to
                        keep your business’s money while it earns interest — whether it’s a business savings account or
                        an
                        investment program. All of these savings options are all FDIC-insured.</p>
                    <p>‍</p>
                    <h3>Business Money Market Account</h3>
                    <p>{{config('app.name')}} Bank’s Business Money Market Account offers one of the smartest, safest, and most secure
                        ways to
                        grow your business’s money. This is similar to a savings account but with higher interest rates.
                        These
                        earnings come without the risk of investing through stocks, bonds, and Money Market Mutual
                        Funds, which are
                        tied to market fluctuations. This type of savings option is backed by FDIC to the maximum amount
                        allowed by
                        the law.</p>
                    <p>The benefit of having a Business Money Market Account is two-fold. Competitive interest rates
                        offer higher
                        annual percentage yields and you can more easily access that money. A rainy-day fund also serves
                        as an
                        emergency reserve. You can withdraw from this account up to six times a month without being
                        penalized. The
                        account can even provide overdraft protection by sweeping funds into your business checking
                        account as
                        needed. Managing your Business Money Market Account’s transactions and activity can be done
                        through our
                        online banking services. Talk to one of our bankers about opening a Business Money Market
                        Account for as
                        little as $2,500.</p>
                    <p>‍</p>
                    <h3>Opening a Business Money Market Account</h3>
                    <ul>
                        <li>FDIC insures your Business Money Market up to $250,000</li>
                        <li>Account will not earn interest for any day the balance is below $2,500</li>
                        <li>A $15 maintenance charge will be assessed if the account falls below $2,500</li>
                        <li>Limit of six withdrawals per month</li>
                        <li>A $5 fee may apply to each additional transaction</li>
                    </ul>
                    <p>‍</p>
                    <h3>{{config('app.name')}} Bank CDs (Certificates of Deposit)</h3>
                    <p>Investing in CDs may be the right choice for your business if it can part with money over a
                        certain period
                        of time. CDs carry minimal risk and give your business’s money a guaranteed return.</p>
                    <p>A CD is a federally insured savings account with a fixed interest rate and a fixed date of
                        withdrawal, also
                        called the maturity date. {{config('app.name')}} Bank issues CDs in promissory notes typically between three
                        months and
                        five years. These earn higher interest rates than a savings account. The {{config('app.name')}} Bank team will
                        work with
                        you to determine which plan is best for your business’s savings goals. When the note matures,
                        the entire
                        amount, including the initial deposit and principle, is available for withdrawal.</p>
                    <p>CDs are considered safer investing options. Even though the return comes more slowly, the funds
                        are backed
                        by the federal government —  unlike a similar investment in the stock market.</p>
                    <p>Opening a CD differs from a savings account in terms of accessing your funds. CDs come with a
                        commitment
                        that you will keep your funds in your account for a determined term length. Withdrawing that
                        money early can
                        result in penalty fees. Talk with one of our bankers about a time commitment that works for your
                        business.
                    </p>
                    <p> </p>
                    <h3>{{config('app.name')}} Bank Specialty CDs</h3>
                    <p>Rates and terms on your deposit will vary depending on the amount of the deposit and term length
                        of your
                        account.</p>
                    <ul>
                        <li>Minimum required deposit</li>
                        <li>Withdrawing before maturation date can result in penalty fees</li>
                        <li>FDIC does not cover penalties incurred from early withdrawals</li>
                    </ul>
                    <p>For more information please call or visit any one of our branch locations during business hours;
                        contact
                        information listed at the bottom of this page.<br/></p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
