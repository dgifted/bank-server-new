@extends('layouts.landing-master')
@section('page-title', 'CDARS')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank is a CDARS member</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-888ac2f7be42-c8973e10"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2603de6c9788129d853f97_CDARS.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>{{config('app.name')}} Bank is your local CDARS® (Certificate of Deposit Account Registry Service) network
                        member. CDARS
                        is a one-stop shop for businesses, individuals, and other organizations who want to conveniently
                        and
                        securely access multi-million-dollar FDIC insurance on CD investments. This service streamlines
                        the
                        complicated nature of keeping your investments under FDIC protection within a single
                        relationship. {{config('app.name')}}
                        Bank, the financial institution you already trust, handles all aspects of this service from
                        negotiating a
                        rate to consolidating statements for easy review.</p>
                    <h3>How CDARS could be right for you</h3>
                    <p>CDARS® could be your longer-term investment tool if you or your business has FDIC insurance needs
                        over
                        $250,000 or if you have several CD accounts. {{config('app.name')}} Bank can simplify your CD account
                        management system by
                        placing large deposits into smaller denominations below the standard FDIC insurance maximum. We
                        then
                        disperse those deposits into other CDARS Network member banks which are all FDIC financial
                        institutions.
                        Those deposits become eligible for FDIC insurance.</p>
                    <p>If you find yourself negotiating multiple interest rates, CDARS allows you to skip over that
                        process.
                        Customers negotiate one interest rate per maturity on CD investments. You’ll never sort through
                        or manually
                        combine your statements again. All of your holdings are consolidated into one, regular,
                        easy-to-read
                        statement.</p>
                    <h3>CDARS is safe and confidential</h3>
                    <p>Rates on CDs may compare favorably with other high-quality, fixed-income investments, such as
                        Treasury
                        securities. In CDARS’ history, no deposit has lost a single cent on a FDIC-insured deposit.</p>
                    <p>We guarantee your account information is protected because it stays between you and {{config('app.name')}}
                        Bank. We are
                        the only financial institution working with you on your CD account needs. CDs with other banks
                        are
                        identified only by a tracking number and your personal information remains confidential.</p>
                    <h3>Convenience, no hidden fees</h3>
                    <p>CDARS eliminates the ongoing and time-consuming chore of tracking changing collateral values.
                        This is
                        possible because your deposits fall under the protection of FDIC. You also have access to a wide
                        variety of
                        maturities — ranging from four weeks to five years.</p>
                    <p>We promise: No hidden fees. When you use CDARS, you are only working with us — and we won’t be
                        surprising
                        you with annual fees, subscription fees, or transaction fees.</p>
                    <h3>CDARS works in five simple steps</h3>
                    <ol>
                        <li><strong>Submit an order<br/></strong>{{config('app.name')}} Bank submits an order to request placement of
                            your money.
                        </li>
                        <li><strong>CDs issued through the CDARS system<br/></strong>Your money is placed into CDs with
                            denominations under $250,000, which are then issued by other banks who are CDARS members.
                            You can identify
                            banks where you do not wish your funds to be placed and you can also review a list of banks
                            where CDARS
                            proposes to place your deposits. This allows you to ensure your entire deposit is fully
                            insured.
                        </li>
                        <li><strong>Confirmation of deposit<br/></strong>When your CDs are issued, we will send you a
                            notice
                            confirming your deposit.
                        </li>
                        <li><strong>Interest paid directly to you<br/></strong>{{config('app.name')}} Bank issues your consolidated
                            interest
                            payments directly to you or in a designated account.
                        </li>
                        <li><strong>Monthly statement<br/></strong>Each month, we send you one statement listing all the
                            banks
                            issuing the CDs, maturity dates, interest earned, and other details.
                        </li>
                        <li><strong>Annual 1099 tax summary<br/></strong>At the end of each year, we send you a
                            consolidated 1099
                            tax summary that reports your taxable interest income. Learn more about CDARS and whether it
                            is the right
                            service for you on their website at <a href="http://www.cdars.com/" target="_blank">www.cdars.com</a>.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
