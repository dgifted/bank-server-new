@extends('layouts.landing-master')
@section('page-title', 'Blog')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside leadership">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">BLOG</h1>
        </div>
    </div>

    <div id="section2" class="section120-white">
        <div class="container1200">
            <div class="w-dyn-list">
                <div class="w-dyn-items">
                    <div data-w-id="b78bf0e5-da40-4e25-4373-6fc253b73570" class="w-dyn-item"><a
                            class="blog-listing w-inline-block">
                            <div class="blog-meta-container">
                                <h3 class="text-date">January 3, 2020</h3>
                                <h2 class="blog-title">Why a Tulsa business owner moved all of her accounts from a
                                    &#x27;mega bank&#x27;
                                    to {{config('app.name')}} Bank</h2>
                                <div class="button">Read more →</div>
                            </div>
                            <div
                                style="background-image:url(_https_/assets.website-files.com/5def52e29f7f6722d7ea071a/5df8f97c5e78c075e0809911_hero-fox-cleaners.html)"
                                class="blog-thumb"></div>
                        </a></div>
                    <div data-w-id="b78bf0e5-da40-4e25-4373-6fc253b73570" class="w-dyn-item"><a
                            class="blog-listing w-inline-block">
                            <div class="blog-meta-container">
                                <h3 class="text-date">December 10, 2019</h3>
                                <h2 class="blog-title">How {{config('app.name')}} Bank helped a Tulsa entrepreneur launch a Top
                                    Manufacturing Company
                                </h2>
                                <div class="button">Read more →</div>
                            </div>
                            <div
                                style="background-image:url(_https_/assets.website-files.com/5def52e29f7f6722d7ea071a/5defb1dd0244b43a38f34e0e_hero-big-sky-energy.html)"
                                class="blog-thumb"></div>
                        </a></div>
                    <div data-w-id="b78bf0e5-da40-4e25-4373-6fc253b73570" class="w-dyn-item"><a
                            class="blog-listing w-inline-block">
                            <div class="blog-meta-container">
                                <h3 class="text-date">December 10, 2019</h3>
                                <h2 class="blog-title">How {{config('app.name')}} Bank gets creative for business customers</h2>
                                <div class="button">Read more →</div>
                            </div>
                            <div
                                style="background-image:url(_https_/assets.website-files.com/5def52e29f7f6722d7ea071a/5defb1e520b40f3b4dc2de84_hero-adorn.html)"
                                class="blog-thumb"></div>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
