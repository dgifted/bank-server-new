@extends('layouts.landing-master')
@section('page-title', 'Security')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside leadership">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Security</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav"><a href="#" class="nav-link-sidebar">Privacy Policy</a><a
                    href="{{route('security')}}" class="nav-link-sidebar w--current">Security</a><a href="#"
                                                                                            class="nav-link-sidebar">Terms
                    of Use</a><a href="#" class="nav-link-sidebar">USA Patriot
                    Act</a></div>
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <h2><strong>FDIC Consumer Alert</strong></h2>
                    <p><strong>A Bank Customer’s Guide to Cybersecurity </strong>has been released by FDIC. The world is
                        changing
                        to a “cyber” world for everything from shopping and communicating to banking and bill paying.
                        While this is
                        more convenient for many people, it also poses additional risks that all consumers need to
                        understand so
                        that they can take the necessary precautions.</p>
                    <p>Please read the complete guide at: <a
                            href="https://www.fdic.gov/consumers/consumer/news/cnwin16/FINAL_Color_CN_Winter2016.pdf"
                            target="_blank">FDIC Consumer Info</a>.</p>
                    <p><strong>FTC Consumer Alert</strong></p>
                    <p><strong>How Not to Get Hooked by a ‘Phishing’ Scam</strong></p>
                    <p>Internet scammers casting about for people’s financial information have a new way to lure
                        unsuspecting
                        victims: They go “phishing” .</p>
                    <p>Phishing is a high-tech scam that uses spam or pop-up messages to deceive you into disclosing
                        your credit
                        card numbers, bank account information, Social Security number, passwords, or other sensitive
                        information.
                    </p>
                    <p>According to the Federal Trade Commission (FTC), phishers send an email or pop-up message that
                        claims to be
                        from a business or organization that you deal with – for example, your Internet Service Provider
                        (ISP),
                        bank, online payment service, or even a government agency. The message usually says that you
                        need to
                        “update” or “validate” your account information. It might threaten some dire consequence if you
                        don’t
                        respond. The message directs you to a Web site that looks just like a legitimate organization’s
                        site, but it
                        isn’t. The purpose of the bogus site? To trick you into divulging your personal information so
                        the operators
                        can steal your identity and run up bills or commit crimes in your name.</p>
                    <p>The FTC, the nation’s consumer protection agency, suggests these tips to help you avoid getting
                        hooked by a
                        phishing scam:</p>
                    <ul>
                        <li>If you get an email or pop-up message that asks for personal or financial information, do
                            not reply or
                            click on the link in the message. Legitimate companies don’t ask for this information via
                            email. If you
                            are concerned about your account, contact the organization in the email using a telephone
                            number you know
                            to be genuine, or open a new Internet browser session and type in the company’s correct Web
                            address. In
                            any case, don’t cut and paste the link in the message.
                        </li>
                        <li>Don’t email personal or financial information. Email is not a secure method of transmitting
                            personal
                            information. If you initiate a transaction and want to provide your personal or financial
                            information
                            through an organization’s Web site, look for indicators that the site is secure, like a lock
                            icon on the
                            browser’s status bar or a URL for a website that begins “https:” (the “s” stands for
                            “secure”).
                            Unfortunately, no indicator is foolproof; some phishers have forged security icons.
                        </li>
                        <li>Review credit card and bank account statements as soon as you receive them to determine
                            whether there
                            are any unauthorized charges. If your statement is late by more than a couple of days, call
                            your credit
                            card company or bank to confirm your billing address and account balances.
                        </li>
                        <li>Use anti-virus software and keep it up to date. Some phishing emails contain software that
                            can harm your
                            computer or track your activities on the Internet without your knowledge. Antivirus software
                            and a
                            firewall can protect you from inadvertently accepting such unwanted files. Antivirus
                            software scans
                            incoming communications for troublesome files. Look for anti-virus software that recognizes
                            current
                            viruses as well as older ones; that can effectively reverse the damage; and that updates
                            automatically. A
                            firewall helps make you invisible on the Internet and blocks all communications from
                            unauthorized sources.
                            It’s especially important to run a firewall if you have a broadband connection. Finally,
                            your operating
                            system (like Windows or Linux) may offer free software “patches” to close holes in the
                            system that hackers
                            or phishers could exploit.
                        </li>
                        <li>Be cautious about opening any attachment or downloading any files from emails you receive,
                            regardless of
                            who sent them.
                        </li>
                        <li>Report suspicious activity to the FTC. If you get spam that is phishing for information,
                            forward it to
                            <a href="https://www.ftccomplaintassistant.gov/"
                               target="_blank">FTCComplaintAssistant.gov</a>. If you
                            believe you’ve been scammed, file your complaint at www.ftc.gov, and then visit the FTC’s
                            Identity Theft
                            Web site at <a href="http://www.ftc.gov/idtheft" target="_blank">FTC.gov/IDTheft</a> to
                            learn how to
                            minimize your risk of damage from ID theft. Visit <a href="http://www.ftc.gov/spam"
                                                                                 target="_blank">FTC.gov/spam</a> to
                            learn other ways to avoid email scams and deal with deceptive spam.
                            The FTC works for the consumer to prevent fraudulent, deceptive, and unfair business
                            practices in the
                            marketplace and to provide information to help consumers spot, stop, and avoid them. To file
                            a complaint
                            or to get free information on consumer issues , visit <a href="http://www.ftc.gov/"
                                                                                     target="_blank">FTC.gov</a> or call
                            toll-free, 1-877-FTC-HELP (1-877-382-4357); TTY: 1-866-653-4261. The
                            FTC enters Internet, telemarketing, identity theft, and other fraud-related complaints into
                            Consumer
                            Sentinel, a secure, online database available to hundreds of civil and criminal law
                            enforcement agencies
                            in the U.S. and abroad.
                        </li>
                    </ul>
                    <p><strong>Protect Yourself From Identity Theft </strong><br/>Think of how many times a day you
                        share your
                        personal information. You may write a check at the local grocery store, apply for a credit card,
                        make a call
                        on your cell phone, charge tickets to a Milwaukee Bucks game, mail your tax return or buy
                        Midwest Express
                        tickets over the Internet.</p>
                    <p>With each transaction, you share your personal information: your bank and credit card account
                        numbers, your
                        income, your social security number, your name, address and phone number.</p>
                    <p>In 1998, Congress passed a law making identity theft a federal crime. The U.S. Secret Service,
                        FBI and U.S.
                        Postal Inspection Service investigate violations of the Act. Persons accused of identity theft
                        are
                        prosecuted by the Department of Justice.</p>
                    <p>Wisconsin also has passed legislation making identity theft a felony, and criminals here have
                        been
                        convicted of the crime.</p>
                    <p>Consumer complaints about identity theft continue to grow. More than 40 percent of all complaints
                        filed
                        with the U.S. Federal Trade Commission last year were for identity theft.</p>
                    <p>Unless you live your life in a bubble, you can’t prevent the stealing of your personal
                        information, but you
                        can minimize the risks of this crime happening to you by following these suggestions:</p>
                    <ul>
                        <li>Never divulge information about your social security number, credit card number, account
                            passwords and
                            other personal information unless you initiate contact with a person or company you know and
                            trust.
                        </li>
                        <li>Don’t carry around more checks, credit cards and other bank items than you really need.
                            Don’t carry your
                            social security number in your wallet, and be sure to pick passwords and PINs (Personal
                            Identification
                            Numbers) that will be tough for someone to figure out. Don’t write your social security
                            number on your
                            check.
                        </li>
                        <li>Protect your incoming and outgoing mail, especially envelopes that may contain checks,
                            credit card
                            applications or other information valuable to a fraud artist. Deposit outgoing mail,
                            especially something
                            containing personal financial information in the official Post Office collection boxes, hand
                            it to the
                            mail carrier, or take it to the local post office instead of leaving it in your home
                            mailbox.
                        </li>
                        <li>Before discarding credit card applications, cancelled checks, bank statements or other
                            information
                            useful to an identity thief, tear them up as best you can, preferably by using a paper
                            shredder.
                        </li>
                        <li>Safely store extra checks, credit cards and documents that list your social security
                            number.
                        </li>
                        <li>Contact your financial institution immediately if you lose your checkbook or bank credit
                            card, if there
                            is a discrepancy in your records, or if you notice something suspicious such as a missing
                            payment or
                            unauthorized withdrawals.
                        </li>
                        <li>If your credit card bill doesn’t arrive on time, contact your credit card company. This
                            could be a sign
                            that someone has stolen your account information, changed your address and is making large
                            charges in your
                            name from another location.
                        </li>
                        <li>Once a year check your credit record with the three major credit bureaus. To order your
                            report, call the
                            following toll-free numbers; Equifax: 800-685-1111 Experian: 888-397-3742 Trans Union:
                            800-888-4213
                        </li>
                    </ul>
                    <p>If you are a victim of identity theft, take the following steps:</p>
                    <ul>
                        <li>Contact the fraud departments of each of the three major credit bureaus and request a “fraud
                            alert” be
                            placed on your file and no new credit be granted without your approval.
                        </li>
                        <li>Close any accounts that have been fraudulently accessed or opened.</li>
                        <li>File a local police report and get a copy of the report to your bank, credit card company or
                            others that
                            may need proof of the crime.
                        </li>
                    </ul>
                    <p>The Federal Trade Commission (FTC) is the federal clearinghouse for complaints by victims of
                        identity
                        theft. Although the FTC does not have the authority to bring criminal cases, it can assist
                        victims by
                        providing information to help resolve problems that can result from identity theft. Should you
                        find yourself
                        a victim of identity theft, you can file a complaint with the FTC by calling toll-free
                        1-877-ID-THEFT
                        (438-4338).</p>
                    <p>Most of us assume that thieves are only interested in the cash in our wallet or purse, when in
                        many cases,
                        they are more interested in access to sensitive information that can be used to steal our
                        identity. Use
                        caution and don’t be the next victim of identity theft or other financial fraud.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
