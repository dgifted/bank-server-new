@extends('layouts.landing-master')
@section('page-title', 'Online Services')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Online Services</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-6e50cab14800-7e44dd7e"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2619ec56fefe0990aa484e_Online Services.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <h2><strong>Online Banking</strong></h2>
                    <p><strong>Your accounts are right at your fingertips with Online Banking from {{config('app.name')}}
                            Bank.</strong></p>
                    <p>Manage your day-to-day finances (24 hours a day, 7 days a week, 365 days a year) from the comfort
                        of home,
                        work or anywhere you have Internet access.</p>
                    <ul>
                        <li>Get account information</li>
                        <li>View statements</li>
                        <li>Search recent transactions</li>
                        <li>Transfer funds between accounts</li>
                        <li>Pay bills</li>
                        <li>Deposit Checks</li>
                        <li>Make loan payments</li>
                    </ul>
                    <h2><strong><br/>e-Statements</strong></h2>
                    <p><strong>View your monthly checking account statements online with our free electronic
                            statements!</strong>
                    </p>
                    <p>We will send you an e-mail when your statement is ready. To view your statement, simply open the
                        PDF
                        attachment and log on to our secure site using your password. In addition to eliminating the
                        risk of paper
                        statements being lost or stolen in the mail, you will also be able to:</p>
                    <ul>
                        <li>View, print and save statements right to your computer</li>
                        <li>View check images</li>
                        <li>eStatements are paperless, so they are environmentally friendly!</li>
                    </ul>
                    <h2><strong><br/>Online Bill Pay</strong></h2>
                    <p><strong>No Checks, No Stamps, No Envelopes. Now there’s a better way to pay your bills!</strong>
                    </p>
                    <p>Imagine paying all of your monthly bills – from your mortgage to your credit cards – online. You
                        can pay
                        bills immediately or on a recurring basis. It’s quick, it’s easy and it’s secure. You’ll
                        probably wonder why
                        you paid bills the “old fashioned” way for so long.</p>
                    <p>‍</p>
                    <h2>24-Hour Bank-By-Phone</h2>
                    <h4><strong>‍</strong>‍<strong>Access your accounts easily anytime, anywhere with our 24-hour
                            Bank-by-Phone
                            (877) 970-2265. It is free, secure, and convenient!</strong></h4>
                    <p>Here are just a few of the many things you can do with Bank-by-Phone. Follow simple voice mail
                        instructions
                        to:</p>
                    <ul>
                        <li>Check account balances</li>
                        <li>Review recent transactions</li>
                        <li>See if a check has cleared</li>
                        <li>Transfer funds</li>
                        <li>Make loan payments</li>
                    </ul>
                    <p><br/></p>
                    <h2><strong>Mobile Banking </strong></h2>
                    <p><strong>{{config('app.name')}} Bank’s free mobile banking service is here!</strong></p>
                    <p>Take care of just about everything you would at the branch, but on your phone – check your
                        current balance,
                        transfer funds, pay bills, make loan payments and more!</p>
                    <p>You must be enrolled in online banking first and you will use the same username and password to
                        sign on to
                        mobile banking.</p>
                    <p>*Wireless carrier data rates may apply.</p>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
