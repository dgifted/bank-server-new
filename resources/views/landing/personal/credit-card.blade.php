@extends('layouts.landing-master')
@section('page-title', 'Personal Credit Card')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Personal Credit Card</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-ea78ef2a58e6-f9fcec27"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2619886c9788330e86151a_Personal Credit Card.jpg"
                                alt="" /></div>
                    </figure>
                    <p>‍</p>
                    <p><strong>We make it easy to choose the credit card that is right for you – Low Rate or Preferred Points
                            Rewards. It’s the only card you need for everything you need to do.  </strong><em>Available to all
                            applicants for both MasterCard® and Visa®</em></p>
                    <p>‍</p>
                    <h2><strong>Personal Card – Low Rate</strong></h2>
                    <p>‍</p>
                    <p><strong>Features</strong></p>
                    <ul>
                        <li>No annual fee.</li>
                        <li>Low introductory rate for the first six months.1</li>
                        <li>Competitive ongoing APR. 1</li>
                        <li>25-day interest-free grace period on all purchases. No grace period on cash advances.</li>
                    </ul>
                    <p>‍</p>
                    <p><strong>Benefits</strong></p>
                    <ul>
                        <li>24-hour toll-free live customer assistance available at 800-367-7576.</li>
                        <li>Online account information available 24/7.</li>
                        <li>Rental car collision damage waiver protection and a host of extraordinary MasterCard®</li>
                        <li>Unlike some card offers, the APR1 for purchases and cash advances is the same.</li>
                    </ul>
                    <p>‍</p>
                    <p><strong> Fees</strong></p>
                    <ul>
                        <li>Other fees may be charged, these fees are listed on the <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e62d66a191f2ac13b0dee81_4633-MC-07-2019-CON.pdf"
                                target="_blank" data-goodbye-skip="1">Personal Application</a>.</li>
                    </ul>
                    <p>‍</p>
                    <h2><strong>Personal Card –World / Preferred Points Rewards</strong></h2>
                    <p>‍</p>
                    <p><strong> Features</strong></p>
                    <ul>
                        <li>Earn points that you can redeem for rewards. Earn one point for each dollar spent, up to 10,000 points
                            per month.</li>
                        <li>Low introductory rate for first six months. 1</li>
                        <li>Competitive ongoing APR. 1</li>
                        <li>No annual fee.</li>
                        <li>25-day interest-free grace period on all purchases. No grace period on cash advances.</li>
                    </ul>
                    <p>‍</p>
                    <p><strong> Benefits</strong></p>
                    <ul>
                        <li>Redeem your rewards points for cash-back awards, retail gift cards, travel and a wide variety of
                            merchandise including cameras, mp3 players, home theater systems, portable DVD players, sporting
                            equipment, jewelry, luggage, electronics, video game equipment, gift cards and more.</li>
                        <li>To view or redeem rewards points.</li>
                        <li>24-hour toll-free live customer assistance.</li>
                        <li>Online account information available 24/7 .</li>
                        <li>Rental car collision damage waiver protection and a host of extraordinary MasterCard® benefits.</li>
                        <li>Unlike some card offers, the APR 1 for purchases and cash advances is the same.</li>
                    </ul>
                    <p>‍</p>
                    <p><strong> Fees</strong></p>
                    <ul>
                        <li>Other fees may be charged, these fees are listed on the downloadable application. <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e62d66a191f2ac13b0dee81_4633-MC-07-2019-CON.pdf"
                                target="_blank" data-goodbye-skip="1">Personal Application</a></li>
                    </ul>
                    <p>‍</p>
                    <p><strong>To Apply Now</strong></p>
                    <ul>
                        <li>Download the PDF application. <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e62d66a191f2ac13b0dee81_4633-MC-07-2019-CON.pdf"
                                target="_blank" data-goodbye-skip="1">Personal Application</a></li>
                        <li>Type the information and then print the application (you cannot save the document, so print an extra
                            copy for yourself), or print it and then complete it using a blue or black pen. Paper applications are
                            also available at your local branch office.</li>
                        <li>Fax your completed application to {{config('app.name')}} Bank at 918-712-4799 or mail your completed application to
                            {{config('app.name')}} Bank, P.O. Box 52490, Tulsa, OK  74152</li>
                    </ul>
                    <p>‍</p>
                    <p><em>1 Please see application for information about current APRs and fees.</em></p>
                    <p><em>2 Concierge Services available with World MasterCard® only.</em></p>
                    <p><em>3The creditor, issuer and service provider of these credit cards is TIB The Independent BankersBank,
                            N.A., not {{config('app.name')}} Bank, pursuant to separate licenses from Visa® U.S.A. Inc. and MasterCard®International
                            Incorporated.  MasterCard® is a registered trademark of MasterCard® International Incorporated. Credit
                            card products are subject to credit approval by TIB The Independent BankersBank, N.A. </em></p>
                    <p><em>4 Late payments and going over the credit limit may damage your credit history.</em></p>
                    <p>‍</p>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
