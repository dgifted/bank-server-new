@extends('layouts.landing-master')
@section('page-title', 'Savings and Investments')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank Personal Savings and Investments</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings attached.</li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever suggesting ideas
                            and solutions.</li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure you clearly
                            understand the advantages and disadvantages of each option.</li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete with other
                            solutions you’ve shopped.</li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities, we’ll offer
                            our expert opinion on your other options.</li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get back to you
                            within the next business day, if not sooner.</li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-a41857268e6f-51fce1e1"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2619080574ee4b98777056_Saving and Investments.jpg"
                                alt="" /></div>
                    </figure>
                    <p>‍</p>
                    <p>Whether you’re saving money to make a purchase in the near future, or you have a long-term  savings goal,
                        putting your money aside is important.</p>
                    <p>{{config('app.name')}} Bank has a savings option to help you reach those financial goals. Contact us today to explore
                        your options.</p>
                    <h2>Savings Accounts</h2>
                    <p>Savings accounts are federally insured accounts that can help you meet long-term goals of saving, such as
                        buying real estate. Using a savings account allows you to create distance between short-term, everyday
                        spending money that’s easily accessible in your checking account, and money that’s reserved for a later
                        date, like vacation savings or a large purchase.</p>
                    <p><strong>Opening a {{config('app.name')}} Bank Savings Account</strong></p>
                    <ul>
                        <li>Minimum deposit of $100 to open</li>
                        <li>All withdrawals over three per month are assessed a $1-per-item charge.</li>
                        <li>Interest is calculated on the daily balance, based on posted rates, and paid quarterly. If the balance
                            in the account falls below $100, a $3 service charge for that month will be assessed. Additionally, the
                            account will not earn interest for any day the balance is below $100.</li>
                    </ul>
                    <h2>Special Savings Account</h2>
                    <p>{{config('app.name')}} Bank customers who are 17 years and younger, as well as customers 62 years and older, can open a
                        special savings account. This account requires only a $1 minimum balance to open.</p>
                    <h2>Money Market Accounts</h2>
                    <p>Money market accounts are interest-bearing accounts that typically pay a higher interest rate than savings
                        accounts. Because they also allow account holders to write checks, money market accounts are able to offer
                        the account holder the benefits they would receive from both savings and checking accounts.</p>
                    <p><strong>Opening a {{config('app.name')}} Bank Money Market Account</strong></p>
                    <ul>
                        <li>Minimum deposit of $2,500 to open</li>
                        <li>Tiered interest rates (subject to change daily)</li>
                        <li>Limit of 6 withdrawals per month</li>
                        <li>A fee of $5 per item may apply to such transactions over the federally regulated limit of six. If the
                            balance in the account falls below $2,500, a $15 service charge, for that month, will be assessed.
                            Additionally, the account will not earn interest for any day the balance is below $2,500.</li>
                    </ul>
                    <h2>CDs (Certificates of Deposit)</h2>
                    <p>Certificates of deposit, or CDs, are federally insured savings accounts with fixed interest rates and a
                        fixed date of withdrawal. CDs are different from traditional savings accounts in one key area: savings
                        accounts let you deposit and withdraw funds relatively freely. However, with a CD, you agree to leave your
                        money in the bank for a set amount of time. During this time, you can’t access the funds without paying a
                        penalty. The longer you commit to keeping your money in that account without touching it, the higher the
                        interest rate you’ll earn. </p>
                    <p>CDs are best for account holders who are certain that they won’t need to withdraw those funds for the
                        duration of the term length. </p>
                    <p><strong>Opening a CD (Certificates of Deposit) at {{config('app.name')}} Bank</strong></p>
                    <p>Rates and terms of your CD account will vary depending on the amount of your deposit and the term length of
                        your account. </p>
                    <p>Talk to a New Accounts Representative today to open a CD account:</p>
                    <ul>
                        <li>Visit any of our four branch locations during business hours whose addresses are listed at the bottom of
                            the page</li>
                        <li>Call us at any of the four branch locations during business hours whose phone numbers are listed at the
                            bottom of the page</li>
                    </ul>
                    <h2>IRAs (Individual Retirement Accounts)</h2>
                    <p>IRAs are accounts that allow account holders to save for retirement. They allow you to invest your money in
                        stocks, bonds, mutual funds, and other products. Because IRAs differ on how and when your money is taxed,
                        you can save thousands of dollars in tax benefits over the account’s lifespan.</p>
                    <p><strong>Traditional IRAs:</strong> These accounts have an upfront tax deduction. Contributions made to
                        traditional IRAs are tax-deductible in the year they are made.</p>
                    <p><strong>Roth IRAs:</strong> Withdrawals from Roth IRAs in retirement are not taxed. These accounts have
                        fewer restrictions for retirees, and make it easier to pass funds to your family members. </p>
                    <p>{{config('app.name')}} Bank offers both Traditional IRAs and Roth IRAs. We place all of our IRA accounts into CDs
                        (Certificates of Deposit) to maximize growth with FDIC insurance.</p>
                    <p>Whether you are making a normal yearly contribution, a transfer, or rollover, {{config('app.name')}} Bank has custom
                        solutions to fit your IRA needs.</p>
                    <p>Contact us to learn more about {{config('app.name')}} Bank’s savings products and services and start investing your money
                        today.<strong><br /></strong></p>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
