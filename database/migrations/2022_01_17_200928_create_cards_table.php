<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('account_id')->constrained();
            $table->tinyInteger('is_active')->default(\App\Models\Card::STATUS_ACTIVE);
            $table->string('number')->unique();
            $table->string('pin');
            $table->string('expires_month');
            $table->string('expires_year');
            $table->string('daily_online_limit')->nullable();
            $table->string('daily_withdrawal_limit')->nullable();
            $table->string('monthly_online_limit')->nullable();
            $table->string('monthly_withdrawal_limit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
