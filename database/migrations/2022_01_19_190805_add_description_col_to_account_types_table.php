<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionColToAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_types', function (Blueprint $table) {
            $table->text('description')->nullable();
        });

        if (config('app.env') === 'local') {
            $faker = Faker\Factory::create();

            \App\Models\AccountType::all()
                ->each(function ($accType) use ($faker) {
                    $accType->description = $faker->paragraph(random_int(2, 5));
                    $accType->save();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_types', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
