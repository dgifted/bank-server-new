<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $usersId = \App\Models\User::all('id')->pluck('id')->toArray();
        $senderId = $this->faker->randomElement($usersId);
        $receiverId = $this->pickRandomReceiver($usersId, $senderId);

        return [
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'title' => $this->faker->sentence(random_int(4, 10)),
            'content' => $this->faker->paragraph(random_int(2, 4)),
            'is_read' => $this->faker->randomElement([
                Message::STATUS_UNREAD,
                Message::STATUS_READ
            ])
        ];
    }

    private function pickRandomReceiver($userIds, $lastSenderId)
    {
        $receiverId = $this->faker->randomElement($userIds);
        if ($lastSenderId === $receiverId) {
            $this->pickRandomReceiver($userIds, $lastSenderId);
        }
        return $receiverId;
    }
}
