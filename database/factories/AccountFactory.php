<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $usersId = \App\Models\User::all('id')->pluck('id')->toArray();
        $accountTypesId = \App\Models\AccountType::all('id')->pluck('id')->toArray();
        $currenciesId = \App\Models\Currency::all('id')->pluck('id')->toArray();

        return [
            'user_id' => $this->faker->randomElement($usersId),
            'account_type_id' => $this->faker->randomElement($accountTypesId),
            'currency_id' => $this->faker->randomElement($currenciesId),
            'is_active' => $this->faker->randomElement([
                \App\Models\Account::STATUS_ACTIVE,
                \App\Models\Account::STATUS_INACTIVE
            ]),
            'sort_code' => Str::random(8),
            'number' => \App\Models\Account::generateAccountNumber(),
            'balance' => random_int(100, 99999),
            'ref_id' => \App\Models\Account::generateReferenceId()
        ];
    }
}
