<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransferFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $currIds = \App\Models\Currency::all('id')->pluck('id')->toArray();
        $accts = \App\Models\Account::with('user')->get();
        $srcAcc = $accts->random(1)->first();
        $payer = $srcAcc->user;
        $recAcc = $accts->random(1)->first();
        $payee = $recAcc->user;

        return [
            'currency_id' => $this->faker->randomElement($currIds),
            'source_account_id' => $srcAcc->id,
            'payer_id' => $payer->id,
            'payee_id' => $payee->id,
            'payee_account_id' => $recAcc->id,
            'amount' => random_int(100, 99999),
            'ref_id' => \App\Models\Transfer::generateReferenceID(),
            'status' => $this->faker->randomElement([
                \App\Models\Transfer::STATUS_PENDING,
                \App\Models\Transfer::STATUS_COMPLETED,
                \App\Models\Transfer::STATUS_FAILED,
            ]),
            'type' => $this->faker->randomElement([
                \App\Models\Transfer::TYPE_INCOME,
                \App\Models\Transfer::TYPE_EXPENSE,
            ]),
        ];
    }
}
