<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class
        ]);

        \App\Models\Account::factory()->count(20)->create();
        \App\Models\Card::factory()->count(50)->create();
        \App\Models\Statistic::factory()->count(100)->create();
        \App\Models\Transfer::factory()->count(300)->create();
    }
}
