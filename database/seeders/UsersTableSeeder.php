<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where('email', 'admin@site.io')->first();
        if (!$admin) {
            $admin = User::create([
                'first_name' => 'Site',
                'last_name' => 'Admin',
                'email' => 'admin@site.io',
                'username' => 'Administrator',
                'email_verified_at' => now(),
                'password' => Hash::make('adminPa$$'),
                'uid' => User::generateUId()
            ]);
            $adminRole = Role::where('label', Role::ROLE_ADMIN)->first()->id;
            $admin->roles()->attach($adminRole);
        }
//        $clientRole = Role::where('label', Role::ROLE_CLIENT)->first()->id;
//
//        User::factory()->count(10)->create()
//            ->each(function ($user) use ($clientRole) {
//                $user->roles()->attach($clientRole);
//            });
    }
}
