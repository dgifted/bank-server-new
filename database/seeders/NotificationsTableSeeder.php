<?php

namespace Database\Seeders;

use App\Models\Notification;
use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::all()->except(1)
            ->each(function ($user) {
                Notification::factory()->count(random_int(1, 5))->create([
                    'user_id' => $user->id
                ]);
            });
    }
}
