<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\AccountType;
use App\Models\Deposit;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = User::where('email', '<>', config('app.admin_email'))->get();
        return view('admin.customers')->with(['customers' => $customers]);
    }

    public function show($customerUid)
    {
        $customer = User::where('uid', $customerUid)->first();

        if (!$customer)
            throw new ModelNotFoundException('Customer not found');

        $balance = 0;
        $deposits = $customer->deposits()->get();
        $accountType = AccountType::all();

        $customer->accounts()->get()->each(function ($account) use (&$balance) {
            $balance += (double)$account->balance;
        });

        return view('admin.customer-detail')->with([
            'user' => $customer,
            'accountType' => $accountType,
            'balance' => $balance,
            'deposits' => $deposits
        ]);
    }
}
