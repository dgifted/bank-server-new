<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\AccountType;
use Illuminate\Http\Request;

class AccountTypeController extends Controller
{
    public function index()
    {
        $types = AccountType::withCount('accounts')->get();

        return view('admin.account-types')->with([
            'types' => $types
        ]);
    }

    public function show($refId)
    {
        $type = AccountType::where('ref_id', $refId)->firstOrFail();

        return view('admin.account-type-detail')
            ->with([
                'type' => $type
            ]);
    }

    public function create()
    {
        return view('admin.account-type-create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'unique:account_types']
        ]);

        AccountType::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'ref_id' => AccountType::generateReferenceId()
        ]);

        return redirect()
            ->route('admin.account-types')
            ->with([
                'message' => 'Account type created',
                'type' => 'success'
            ]);
    }

    public function update(Request $request, $refId)
    {
        $type = AccountType::where('ref_id', $refId)->firstOrFail();

        if ($request->get('title'))
            $type->title = $request->get('title');
        if ($request->get('description'))
            $type->description = $request->get('description');

        $type->save();
        return back()->with(['message' => 'Account type updated.']);
    }

    public function destroy($refId)
    {
        $type = AccountType::where('ref_id', $refId)->firstOrFail();

        $type->delete();

        return back()->with([
            'message' => 'Account type deleted.',
            'type' => 'success'
        ]);
    }
}
