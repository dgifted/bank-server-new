<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\DepositCreatedMail;
use App\Models\Deposit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DepositController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $deposits = Deposit::with(['user', 'user.accounts.currency'])
            ->latest()
            ->get();

        return view('admin.deposits')->with([
            'deposits' => $deposits,
        ]);
    }

    public function approve($userId, $refId)
    {
        $deposit = Deposit::where('ref_id', $refId)->firstOrFail();
        $user = User::where('uid', $userId)->firstOrFail();

        $success = false;
        DB::transaction(function () use (&$deposit, &$success, $user) {
            $account = $user->accounts()->first();

            $deposit->status = Deposit::STATUS_COMPLETED;
            $deposit->save();

            $account->balance = (float)$account->balance + (float)$deposit->amount;
            $account->save();
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Deposit approval failed.',
                'type' => 'danger'
            ]);

        return redirect()->route('admin.deposits')->with(['message' => 'Deposit approved']);
    }

    public function disApprove($userId, $refId)
    {
        $deposit = Deposit::where('ref_id', $refId)->firstOrFail();
        $user = User::where('uid', $userId)->firstOrFail();

        $success = false;
        DB::transaction(function () use (&$deposit, &$success, $user) {
            $deposit->status = Deposit::STATUS_FAILED;
            $deposit->save();

            $account = $user->accounts()->first();
            if ($deposit->status === Deposit::STATUS_COMPLETED) {
                $account->balance = (float)$account->balance - (float)$deposit->amount;
                $account->save();
            }
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Operation failed.',
                'type' => 'danger'
            ]);

        return redirect()->route('admin.deposits')->with(['message' => 'Deposit declined']);
    }

    public function store(Request $request, $userId)
    {
        $request->validate([
            'amount' => ['required', 'numeric'],
        ]);

        $user = User::where('uid', $userId)->firstOrFail();
        $account = $user->accounts()->first();

        $success = false;
        $deposit = null;

        DB::transaction(function () use (&$success, &$deposit, $request, $user, &$account) {
            $deposit = $user->deposits()->create([
                'amount' => $request->get('amount'),
                'ref_id' => Deposit::generateReferenceID(),
                'status' => Deposit::STATUS_COMPLETED
            ]);

            $account->balance = (float)$account->balance + (float)$deposit->amount;
            $account->save();
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Operation failed. Please try again.',
                'type' => 'danger'
            ]);

        $currency = $account->currency;
        $user->notificationMessages()->create([
            'description' => 'Your account has be credited with ' . $currency->abbreviation . ' ' . $deposit->amount ?? ''
        ]);
        Mail::to($user->email)->send(new DepositCreatedMail($request->get('amount'), $currency->abbreviation, $user));
        return back()->with([
            'message' => 'Account funded.',
            'type' => 'success'
        ]);
    }

    public function deductFromBalance(Request $request, $userId)
    {
        $request->validate([
            'amount' => ['required', 'numeric'],
        ]);

        $user = User::where('uid', $userId)->firstOrFail();
        $account = $user->accounts()->first();

        if ((float)$account->balance < (float)$request->get('amount'))
            return back()->with([
                'message' => 'Insufficient funds.',
                'type' => 'danger'
            ]);

        $success = false;
        DB::transaction(function () use (&$success, $request, &$account) {
            $account->balance = (float)$account->balance - (float)$request->get('amount');
            $account->save();
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Operation failed. Please try again.',
                'type' => 'danger'
            ]);

        $currency = $account->currency;
        $user->notificationMessages()->create([
            'description' => 'There\'s a deduction of ' . $currency->abbreviation . ' ' . $request->get('amount') ?? '' . 'from your account'
        ]);
        // Mail::to($user->email)->send(new DepositCreatedMail($request->get('amount'), $currency->abbreviation, $user));
        return back()->with([
            'message' => 'Operation successful.',
            'type' => 'success'
        ]);
    }
}
