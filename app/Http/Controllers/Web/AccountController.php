<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountController extends Controller
{
    public function index()
    {
        $accounts = Account::with(['accountType', 'user'])->get();

        return view('admin.accounts')->with([
            'accounts' => $accounts
        ]);
    }

    public function show($refId)
    {
        $account = Account::with(['user'])->where('ref_id', $refId)->first();
        if (!$account)
            throw new NotFoundHttpException('Account not found');

        $statuses = [
            'active' => Account::STATUS_ACTIVE,
            'dormant' => Account::STATUS_DORMANT,
            'inactive' => Account::STATUS_INACTIVE,
            'on-hold' => Account::STATUS_ON_HOLD,
            'suspended' => Account::STATUS_SUSPENDED,
        ];

        return view('admin.account-detail')->with([
            'account' => $account,
            'statuses' => $statuses
        ]);
    }

    public function activate($refId)
    {
        $account = Account::where('ref_id', $refId)->firstOrFail();

        $account->is_active = Account::STATUS_ACTIVE;
        $account->save();

        return back()->with(['message' => 'Account activated']);
    }

    public function deActivate($refId)
    {
        $account = Account::where('ref_id', $refId)->firstOrFail();

        $account->is_active = Account::STATUS_INACTIVE;
        $account->save();

        return back()->with(['message' => 'Account deactivated']);
    }

    public function changeStatus(Request $request, $refId)
    {
        $account = Account::where('ref_id', $refId)->firstOrFail();
        $account->status = $request->get('status');
        $account->save();

        return back()->with([
            'message' => 'Account status changed.',
            'type' => 'success'
        ]);
    }
}
