<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $user = $userDetails = auth()->user();
        $accounts = $user->accounts()->get();
        $balance = 0;

        $accounts->each(function ($account) use (&$balance) {
            $balance += $account->balance;
        });

        $messagesCount = $user->receivedMessages()->count();

        $data = [
            $userDetails,
            'stats' => [
                'accsDetails' => [
                    'count' => $accounts->count(),
                    'balance' => $balance,
                ],
                'messagesCount' => $messagesCount
            ]
        ];

        return response()->json((object)$data);
    }

    public function uploadPhoto(Request $request)
    {
        if (!$request->hasFile('photo'))
            return $this->errorResponse('Please choose a valid image file.');

        $user = auth()->user();
        if ($user->passport && Storage::disk('images')->exists($user->passport)) {
            Storage::disk('images')->delete($user->passport);
        }

        $user->passport = $request->file('photo')->store('', 'images');
        $user->save();

        return $this->showOne($user, 201);
    }

}
