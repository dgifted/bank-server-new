<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;

class UserAccountController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
        $accounts = auth()->user()->accounts()->with(['accountType:id,description', 'currency:id,name,abbreviation'])->get();
        return $this->showAll($accounts);
    }

    public function show($accountId)
    {
        $account = auth()->user()->accounts()->get()
            ->filter(function ($acct) use ($accountId) {
                return (int)$acct->id === (int)$accountId;
            });

        if ($account->count() === 0)
            return $this->errorResponse('Account not found.', 404);

        return $this->showOne($account->first());
    }

    public function stats()
    {
        $account = auth()->user()->accounts()->first();
        $data = auth()->user()->transferIns()->get();

        $payload = (object)[
            'accDetails' => $account,
            'data' => $data
        ];

        return response()->json($payload);
    }
}
