<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;

class UserNotificationController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
        $notifications = auth()->user()->notificationMessages()->latest()->get();
        return $this->showAll($notifications);
    }

    public function show($notifyId)
    {
        $notification = $this->findUserNotificationById($notifyId);
        if (!$notification)
            return $this->errorResponse('Notification message not found', 404);

        return $this->showOne($notification);
    }

    public function destroy($notifyId)
    {
        $notification = $this->findUserNotificationById($notifyId);
        if (!$notification)
            return $this->errorResponse('Notification message not found', 404);
        $notification->delete();

        return $this->showOne($notification);
    }

    private function findUserNotificationById($notifyId)
    {
        $notifications = auth()->user()->notificationMessages()->get()
            ->filter(function ($notification) use ($notifyId) {
                return (int)$notification->id === (int)$notifyId;
            });
        if ($notifications->count() === 0)
            return null;

        return $notifications->first();
    }
}
