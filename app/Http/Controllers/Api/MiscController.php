<?php

namespace App\Http\Controllers\Api;

//use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Models\AccountType;
use App\Models\Currency;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class MiscController extends ApiBaseController
{
    public function getAllAccountTypes()
    {
        $accounts = AccountType::all();
        return response()->json($accounts);
    }

    public function getCountries()
    {
        $countries = DB::table('countries')->get(['id', 'name']);
        return response()->json($countries);
    }

    public function getCountryStates($countryId)
    {
        $country = DB::table('countries')->where('id', $countryId)
            ->first(['id', 'name']);

        if (!$country)
            return [];

        $states = DB::table('states')->where('country_id', $countryId)->get(['id', 'name']);

        return response()->json($states);
    }

    public function getCurrencies()
    {
        $currencies = Currency::all();
        return response()->json($currencies);
    }

    public function getSiteSettings()
    {
        $settings = Setting::firstOrCreate([]);
        return response()->json($settings);
    }
}
