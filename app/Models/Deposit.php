<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Deposit extends Model
{
    use HasFactory;

    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_FAILED = 2;

    protected $guarded = ['id'];
    protected $appends = ['status_name'];

    public function getStatusNameAttribute()
    {
        $statusText = '';
        switch ($this->getAttribute('status')) {
            case self::STATUS_PENDING:
                $statusText = 'pending';
                break;
            case self::STATUS_COMPLETED:
                $statusText = 'completed';
                break;
            case self::STATUS_FAILED:
                $statusText = 'failed';
                break;
        }
        return $statusText;
    }

    public static function generateReferenceID()
    {
        return Str::random(16);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
