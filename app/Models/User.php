<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    const SEX_FEMALE = 'female';
    const SEX_MALE = 'male';
    const SEX_OTHERS = 'others';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $appends = ['avatar'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'email_verified_at',
        'password',
        'first_name',
        'last_name',
        'username',
        'phone',
        'dob',
        'street_address',
        'post_code',
        'city',
        'sex',
        'country',
        'occupation',
        'passport',
        'uid'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatarAttribute()
    {
        return !!$this->getAttribute('passport')
            ? asset('images/' . $this->getAttribute('passport'))
            : asset('assets/dist/img/avatar.png');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'user_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'user_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'user_id');
    }

    public static function generateUId()
    {
        return uniqid(time());
    }

    public function isActive()
    {
        return $this->getAttribute('is_active') === self::STATUS_ACTIVE;
    }

    public function isVerified()
    {
        return $this->getAttribute('email_verified_at') !== null;
    }

    public function notificationMessages()
    {
        return $this->hasMany(Notification::class, 'user_id');
    }

    public function messages()
    {
        return Message::where('sender_id', $this->id)
            ->orWhere('receiver_id', $this->id);
    }

    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user_pivot', 'user_id', 'role_id');
    }

    public function sentMessages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function transfers()
    {
        return Transfer::where([
            'payer_id' => $this->getAttribute('id'),
        ]);
    }

    public function transferOuts()
    {
        return $this->hasMany(Transfer::class, 'payer_id');
    }

//    public function transferIns()
//    {
//        return $this->hasMany(Transfer::class, 'payee_id');
//    }
}
