<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class AccountType extends Model
{
    use HasFactory;

    const BASIC = 'basic';
    const PREMIUM = 'premium';
    const GOLD = 'gold';
    const PLATINUM = 'platinum';

    protected $guarded = ['id'];

    public function accounts()
    {
        return $this->hasMany(Account::class, 'account_type_id');
    }

    public static function generateReferenceId()
    {
        return Str::random(16);
    }
}
