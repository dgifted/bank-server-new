<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait ApiResponser {
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code = 422)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        $collection = $this->sortData($collection);
        $collection = $this->paginate($collection);
        return $this->successResponse(['data' => $collection], $code);
    }

    protected function showOne(Model $model, $code = 200)
    {
        return $this->successResponse(['data' => $model], $code);
    }

    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse(['message' => $message], $code);
    }

    protected function paginate(Collection $collection)
    {
        if (request()->has('per_page')) {
            $perPageAttribute = request()->per_page;
        }
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage = $perPageAttribute ?? 10;
        $result = $collection->slice(($page - 1) * $perPage, $perPage)->values();
        $paginated = new LengthAwarePaginator($result, $collection->count(), $perPage, $page, [
            'path' => $this->pathCorrection(LengthAwarePaginator::resolveCurrentPath())
        ]);
        $paginated->appends(request()->all());
        return $paginated;
    }

    protected function filterData (Collection $collection)
    {
        foreach (request()->query() as $query => $value) {
            $collection = $collection->where($query, $value);
        }
        return $collection;
    }

    protected function sortData(Collection $collection)
    {
        if (request()->has('sort_by')) {
            $attributes = request()->sort_by;
            $collection = $collection->sortBy($attributes);
        }
        return $collection;
    }

    private function pathCorrection($path)
    {
        $protocol = config('app.env') === 'local' ? 'http:' : 'https:';
        list ($proto,$_, $host, $append, $resource) = explode('/', $path);
        return implode('/', [$protocol, '', $host, $append, $resource]);
    }

}
