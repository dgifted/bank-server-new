<?php

namespace App\Traits;

trait Utils
{
    public function generateNumber($for = 'account')
    {
        try {
            $number = '';

            if ($for === 'account') {
                $number = random_int(1000000000, 9999999999);
            } else {
                foreach ([0, 1, 2, 3] as $count) {
                    $digitsGroup = random_int(1000, 9999);
                    $number .= $digitsGroup;
                }
            }
            return $number;
        } catch (\Throwable $ex) {
            return false;
        }
    }
}
