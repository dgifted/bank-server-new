<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $account, $password;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $account
     * @param $password
     */
    public function __construct($user, $account, $password)
    {
        $this->account = $account;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = config('app.name') . ' Account Creation';
        return $this->subject($subject)
            ->view('emails.account-create');
    }
}
