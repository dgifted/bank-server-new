<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $amount, $currency, $destinationAccount, $user;

    /**
     * Create a new message instance.
     *
     * @param $amount
     * @param $currency
     * @param $destinationAccount
     */
    public function __construct($amount, $currency, $destinationAccount, $user)
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->destinationAccount = $destinationAccount;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Transfer successful')
            ->view('emails.transfer-completed');
    }
}
