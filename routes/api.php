<?php

use Illuminate\Support\Facades\Route;

Route::prefix('/auth')->group(function () {
    Route::get('/status', [\App\Http\Controllers\Api\AuthController::class, 'getAuthStatus']);
    Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::post('/register', [\App\Http\Controllers\Api\AuthController::class, 'register']);
    Route::get('/user', [\App\Http\Controllers\Api\AuthController::class, 'getUserDetail']);
});

Route::prefix('/account')->group(function () {
    Route::get('/user-accounts', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'index']);
    Route::get('/user-single-account/{id}', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'show']);
    Route::get('/user-stats', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'stats']);
});
Route::prefix('/card')->group(function () {
    Route::get('/user-cards', [\App\Http\Controllers\Api\Users\UserCardController::class, 'index']);
    Route::get('/user-single-card/{id}', [\App\Http\Controllers\Api\Users\UserCardController::class, 'show']);
    Route::post('/{id}/change-pin', [\App\Http\Controllers\Api\Users\UserCardController::class, 'changePin']);
    Route::get('/{id}/activate', [\App\Http\Controllers\Api\Users\UserCardController::class, 'activateCard']);
    Route::get('/{id}/deactivate', [\App\Http\Controllers\Api\Users\UserCardController::class, 'deActivateCard']);
});
Route::prefix('/deposit')->group(function () {
    Route::post('/create', [\App\Http\Controllers\Api\Users\UserDepositController::class, 'store']);
});
Route::prefix('/message')->group(function () {
    Route::get('/user-messages', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'index']);
    Route::get('/user-unread-messages', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'getUnreadReceivedMessage']);
    Route::get('/user-single-message/{id}', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'show']);
    Route::delete('/{id}/delete', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'destroy']);
    Route::post('{id}/toggle-read', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'toggleRead']);
});
Route::prefix('/notification')->group(function () {
    Route::get('/user-notifications', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'index']);
    Route::get('/user-single-notification/{id}', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'show']);
    Route::delete('{id}/delete', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'destroy']);
});
Route::get('/user-profile', [\App\Http\Controllers\Api\Users\UserProfileController::class, 'index']);
Route::post('/upload-photo', [\App\Http\Controllers\Api\Users\UserProfileController::class, 'uploadPhoto']);
Route::prefix('/transfer')->group(function () {
    Route::get('/user-transfers', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'index']);
    Route::get('/user-single-transfer/{id}', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'show']);
    Route::post('/create', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'store']);
    Route::post('/confirm', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'confirm']);
});
Route::get('/account-types', [\App\Http\Controllers\Api\MiscController::class, 'getAllAccountTypes']);
Route::get('/countries/{id}/states', [\App\Http\Controllers\Api\MiscController::class, 'getCountryStates']);
Route::get('/countries', [\App\Http\Controllers\Api\MiscController::class, 'getCountries']);
Route::get('/currencies', [\App\Http\Controllers\Api\MiscController::class, 'getCurrencies']);
Route::get('/site-settings', [\App\Http\Controllers\Api\MiscController::class, 'getSiteSettings']);
